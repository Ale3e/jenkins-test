package org.utils

class Command {
    public static String curl = 'curl -s -o /dev/null -w "%{http_code}" http://www.example.org/'
    public static String ls = 'ls /'
    public static String pwd = 'pwd'

    public static String getCurl () {
        return curl
    }

    public static String getLs () {
        return ls
    }

    public static String getPwd () {
        return pwd
    }
}