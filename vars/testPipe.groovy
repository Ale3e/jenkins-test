#!/usr/bin/groovy

def call(){
    pipeline {
        agent any
        
        stages {
            stage('curl') {
                steps {
                    curlStep(this)
                }
            }
            
            stage('ls') {
                steps {
                    lsStep(this)
                }
            }
            
            stage('pwd') {
                steps {
                    pwdStep(this)
                }
            }
        }
    }
}

// def call(int buildNumber) {
//   if (buildNumber % 2 == 0) {
//     pipeline {
//       agent any
//       stages {
//         stage('Even Stage') {
//           steps {
//             echo "The build number is even"
//           }
//         }
//       }
//     }
//   } else {
//     pipeline {
//       agent any
//       stages {
//         stage('Odd Stage') {
//           steps {
//             echo "The build number is odd"
//           }
//         }
//       }
//     }
//   }
// }